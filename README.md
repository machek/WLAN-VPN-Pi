Raspberry VPN router for OpenVPN or ProtonVPN version
===========

This repo is fork of https://gitlab.com/heurekus/WLAN-VPN-Pi, a few things simplified, a few things adjusted

Modified for Raspberry Pi3 B with built in Wifi, to turn Pi into OpenVPN router to allow share connection to VPN with other devices.


### Installation

Download and prepare Raspbian Stretch Lite from https://www.raspberrypi.org/downloads/ and create empty ssh file in /boot to allow SSH
access to Pi, no need for desktop app support to waste resources.

Start Pi and over SCP copy scripts to Pi (default ssh user pi, password raspberry) 

Change permission on scripts `chmod 700 *.sh`

Now you can have two scripts

* OpenVPN-install-script--Raspian-Stretch.sh - for OpenVPN client installation
* ProtonVPN-install-script-Pi-Raspian-Stretch.sh - ProtonVPN version which uses [ProtonVPN linux cli client](https://protonvpn.com/support/linux-vpn-tool/)


### OpenVPN version

For OpenVPN version you need from your VPN provider .ovpn file and possibly your user name and password.
Replace ./configuration-files/openvpn/client.conf with your file
Update auth.txt with first line username, second password.
In client.conf update line `auth-user-pass /etc/openvpn/auth.txt`


### ProtonVPN version
ProtonVPN installation requires your proton username, password and account type.
File `/etc/wifipi-start.sh` contains line using ProtonClient to connect to selected country or server
`su pi -c "sudo -i /usr/bin/protonvpn-cli -cc uk"` where parameter `-cc uk` means country UK.
[see documentation for other options](https://protonvpn.com/support/linux-vpn-tool/)

### Wifi AP configuration 
File `hostapd.conf.pi3` contains lines 
* ssid - for network name
* wpa_passphrase - for password
