#!/bin/sh 

logger "VPN Wifi Pi - Now starting all components"

# Give all other service the chance to finish their startup procedures
sleep 5

logger "VPN Wifi Pi - done waiting"

sudo service isc-dhcp-server restart

sudo iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
sudo iptables -A FORWARD -i tun0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o tun0 -j ACCEPT

sudo service dnsmasq restart

logger "VPN Wifi Pi - done..."

logger "ProtonVPN"

su pi -c "sudo -i /usr/bin/protonvpn-cli -cc uk"


